/**************************
 * Get HTML canvas elements
 **************************/
const maincanvas = document.getElementById('gamearena');
const maincontext = maincanvas.getContext('2d');

const bullpencanvas = document.getElementById('bullpen');
const bullpencontext = bullpencanvas.getContext('2d');

/******************
 * Gamepiece object
 ******************/
const gamepiece = {position: {x: 0, y: 0}, matrix: null};
const bullpenpiece = {position: {x: 0, y: 0}, matrix: null};

/*******************
 * Canvas attributes
 *******************/
const ga = {h: maincanvas.height, w: maincanvas.width, c: "#333"};
const bp = {h: bullpencanvas.height, w: bullpencanvas.width, c: "#666"};

/**********************
 * Canvaas construction
 **********************/
const gamearena = canvas(20, 10);
const bullpen = canvas(6, 5);

/*******************
 * Colors for pieces
 *******************/
const colors = [null, '#FF0D72', '#0DC2FF', '#0DFF72', '#F538FF', '#FF8E0D', '#FFE138', '#3877FF'];

var animation;
var standby;

/*****************
 * Starting Values
 *****************/
var score = 0;
var level = 1;
var dropCounter = 0;
var dropSpeed = 1000;
var time = 0;

maincontext.scale(20, 20);
bullpencontext.scale(20, 20);

/*************************
 * Draw initial game piece
 * and start the game
 *************************/
loadBullpen();
initiateNewGamePiece(assignPiece());
run();

/*****************************
 * Handles amination!
 * 
 * @param {int} t = delta time
 *****************************/
function run(t = 0)
{
    var newTime = t - time;

    dropCounter += newTime;

    if (dropCounter > dropSpeed)
        dropShape();

    time = t;

    displayScore();
    redrawCanvases();
    animation = requestAnimationFrame(run);
}

animation = requestAnimationFrame(run);

function initiateNewGamePiece(n)
{
    gamepiece.matrix = gamePiece(n);
    gamepiece.position.x = (gamearena[0].length / 2 | 0) - (gamepiece.matrix[0].length / 2 | 0);
    gamepiece.position.y = 0;
    
    if (collision(gamearena, gamepiece))
    {
        gameOver();
    }
}

function loadBullpen()
{
    standby = assignPiece();
    
    bullpencontext.fillStyle = bp.c;
    bullpencontext.fillRect(0, 0, bp.w, bp.h);
    
    bullpenpiece.matrix = gamePiece(standby);
    
    renderElement(bullpen, {x: 0, y: 0}, bullpencontext);
    renderElement(bullpenpiece.matrix, {x: 1, y: 1.5}, bullpencontext);
}

function assignPiece()
{
    var pieces = 'TJLOSZI';
    return pieces[pieces.length * Math.random() | 0];
}

function redrawCanvases()
{
    maincontext.fillStyle = ga.c;
    maincontext.fillRect(0, 0, ga.w, ga.h);
    
    renderElement(gamearena, {x: 0, y: 0}, maincontext);
    renderElement(gamepiece.matrix, gamepiece.position, maincontext);
}

function renderElement(element, offset, context)
{
    element.forEach(function (row, ypos)
    {
        row.forEach(function (color, xpos)
        {
            if (color !== 0)
            {
                context.fillStyle = colors[color];
                context.fillRect(xpos + offset.x, ypos + offset.y, 1, 1);
            }
        });
    });
}

/***************************************************
 * Clears a complete row and updates the score/level
 ***************************************************/
function clearRow()
{
    var rows = 1;
    
    loop: for (var y = gamearena.length - 1; y > 0; --y)
    {
        for (var x = 0; x < gamearena[y].length; ++x)
            if (gamearena[y][x] === 0)
                continue loop;

        var row = gamearena.splice(y, 1)[0].fill(0);
        gamearena.unshift(row);
        ++y;

        score += rows * 10;
        if (score > 49 * level)
        {
            level += 1;
            if (dropSpeed > 200)
                dropSpeed -= 200;
        }
        rows *= 2;
    }
}

function collision(gamearena, gamepiece)
{
    var matrix = gamepiece.matrix;
    var position = gamepiece.position;
    var arena = gamearena;

    for (var y = 0; y < matrix.length; ++y)
    {
        for (var x = 0; x < matrix[y].length; ++x)
            if (matrix[y][x] !== 0 && (arena[y + position.y] && arena[y + position.y][x + position.x]) !== 0)
                return true;
    }

    return false;
}

function fuse(gamearena, gamepiece)
{
    var matrix = gamepiece.matrix;
    var position = gamepiece.position;
    var arena = gamearena;
    
    matrix.forEach(function(row, y)
    {
        row.forEach(function(column, x)
        {
            if (column !== 0)
                arena[y + position.y][x + position.x] = column;
        });
    });
}

/***************
 * GAME CONTROLS
 ***************/
function shiftShape(offset)
{
    gamepiece.position.x += offset;

    if (collision(gamearena, gamepiece))
        gamepiece.position.x -= offset;
}

function rotateShape(direction)
{
    var matrix = gamepiece.matrix;
    var position = gamepiece.position.x;
    var offset = 1;

    rotate(matrix, direction);

    while (collision(gamearena, gamepiece))
    {
        position += offset;
        offset = -(offset + (offset > 0 ? 1 : -1));

        if (offset > matrix[0].length)
        {
            rotate(matrix, -direction);
            gamepiece.position.x = position;
            return;
        }
    }
}

function dropShape()
{
    gamepiece.position.y++;

    if (collision(gamearena, gamepiece))
    {
        gamepiece.position.y--;
        fuse(gamearena, gamepiece);
        loadBullpen();
        initiateNewGamePiece(standby);
        clearRow();
    }

    dropCounter = 0;
}

function gameOver()
{
    cancelAnimationFrame(animation);
    alert('YOU LOSE!');
    //TODO: create web service track high scores
    
}

function displayScore()
{
    document.getElementById('score').innerText = score;
    document.getElementById('level').innerText = level;
}

/****************************************
 * Base function for rotate right or left
  * 
 * @param {gamepiece.matrix} shape
 * @param {int} direction
*****************************************/
function rotate(shape, direction)
{
    for (var y = 0; y < shape.length; ++y)
        for (var x = 0; x < y; ++x)
            [shape[x][y], shape[y][x]] = [shape[y][x], shape[x][y]];

    if (direction > 0)
        shape.forEach(function(row) { row.reverse(); });
    else
        shape.reverse();
}

/****************************************
 * Resets canvas element with blank space
 * 
 * @param {int} height
 * @param {int} width
 * @return {Array|canvas.space}
 ****************************************/
function canvas(height, width)
{
    var space = [];

    while (height--)
        space.push(new Array(width).fill(0));

    return space;
}

/********************************
 * Creates matrix for game pieces
 * 
 * @param {char} shape
 * @return {Array}
 ********************************/
function gamePiece(shape)
{
    switch (shape)
    {
        case 'I': return [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]]; break;
        case 'J': return [[0, 2, 0], [0, 2, 0], [2, 2, 0]]; break;
        case 'L': return [[0, 3, 0], [0, 3, 0], [0, 3, 3]]; break;
        case 'O': return [[0, 4, 4], [0, 4, 4]]; break;
        case 'S': return [[0, 5, 5], [5, 5, 0], [0, 0, 0]]; break;
        case 'T': return [[0, 6, 0], [6, 6, 6], [0, 0, 0]]; break;
        case 'Z': return [[7, 7, 0], [0, 7, 7], [0, 0, 0]]; break;
    }
}

/*******************
 * Keyboard Commands
 *******************/
document.addEventListener('keydown', function(event)
{
    switch (event.keyCode)
    {
        case 32: /* space; start game */ start(); break;
        case 37: /* left arrow; move left */ shiftShape(-1); break;
        case 39: /* right arrow; move right */ shiftShape(1); break;
        case 40: /* down arrow; drop piece */ dropShape(); break;
        case 90: /* z; rotate left */ rotateShape(-1); break;
        case 88: /* x; rotate right */ rotateShape(1); break;
    }
});
